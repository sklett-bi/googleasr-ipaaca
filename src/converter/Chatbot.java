package converter;

import ipaaca.IpaacaModel;
import ipaaca.LocalMessageIU;

import java.io.InputStream;

public class Chatbot {

	Integer resultIndex;
	public String[] recognitionResults;
	public Double[] confidences;
	public Boolean finalHypo;
	public String timeStamp;

	private InputStream inputStream;

	// every time something is recognized it gets here
	public void wrapResultIntoIU() {

		LocalMessageIU localIU = new LocalMessageIU();
		localIU.setCategory("asrresults");
		localIU.getPayload().put("name", "GoogleASR");
		localIU.getPayload().put("version", "0.10");
		localIU.getPayload().put("protocol", "2");
		localIU.getPayload().put("source", "GoogleASR");

		boolean best = true;

		String bestWords = "";
		String bestConfidenceWords = "";
		String bestTimingWords = "";
		float fakeTime = 0.00f;
		String hypotheses = "";
		String hypothesesConfidences = "";
		for (String recogResult : recognitionResults) {

			hypothesesConfidences += "1.0|";

			String words = "";

			for (String recogWord : recogResult.split(" ")) {
				if (best) {
					bestWords += recogWord + "|";
					bestConfidenceWords += "1.0|";
					bestTimingWords += fakeTime + "|";
					fakeTime += 0.05;
				} else {
					words += recogWord + "|";
				}

			}
			if (best) {
				bestWords = bestWords.substring(0, bestWords.length() - 1);
				bestConfidenceWords = bestConfidenceWords.substring(0,
						bestConfidenceWords.length() - 1);
				bestTimingWords = bestTimingWords.substring(0, bestTimingWords.length() - 1);
			} else {
				words = words.substring(0, words.length() - 1);
			}

			if (!best) {
				hypotheses += words + "||";
			} else {
				localIU.getPayload().put("data", recogResult);
				hypotheses += bestWords + "||";
			}

			best = false;
		}

		hypotheses = hypotheses.substring(0, hypotheses.length() - 2);
		hypothesesConfidences = hypothesesConfidences.substring(0,
				hypothesesConfidences.length() - 1);

		localIU.getPayload().put("best", bestWords);

		localIU.getPayload().put("best_confidence", "1.0");
		localIU.getPayload().put("best_confidence_by_word", bestConfidenceWords);
		localIU.getPayload().put("best_timings", bestTimingWords);

		localIU.getPayload().put("hypotheses", hypotheses);
		localIU.getPayload().put("confidences", hypothesesConfidences);
		// localIU.getPayload().put("confidences_by_word", "value");
		// localIU.getPayload().put("start_time", "value");

		if (getFinalHypo()) {
			localIU.getPayload().put("state", "final");
		} else {
			localIU.getPayload().put("state", "hypothesis");
		}

		IpaacaModel.outBuffer.add(localIU);

	}

	public String speechStarted() {
		return "success";
	}

	public InputStream getInputStream() {
		return inputStream;
	}

	public Integer getResultIndex() {
		return resultIndex;
	}

	public void setResultIndex(Integer resultIndex) {
		this.resultIndex = resultIndex;
	}

	public String[] getRecognitionResults() {
		return recognitionResults;
	}

	public void setRecognitionResults(String[] recognitionResults) {
		this.recognitionResults = recognitionResults;
	}

	public Boolean getFinalHypo() {
		return finalHypo;
	}

	public void setFinalHypo(Boolean finalHypo) {
		this.finalHypo = finalHypo;
	}

	public Double[] getConfidences() {
		return confidences;
	}

	public void setConfidences(Double[] confidences) {
		this.confidences = confidences;
	}

	public String getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

}
