package server;

import ipaaca.Initializer;
import ipaaca.IpaacaModel;
import ipaaca.OutputBuffer;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

import converter.Chatbot;

public class Server {

	public static void main(String... args) throws IOException {

		@SuppressWarnings("resource")
		ServerSocket server = new ServerSocket(8080);

		Initializer.initializeIpaacaRsb();
		IpaacaModel.outBuffer = new OutputBuffer("GoogleASR");

		while (true) {
			Socket s = server.accept();

			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(
					s.getInputStream(), "UTF8"));
			char[] buffer = new char[4 * 1024];
			bufferedReader.read(buffer, 0, 4 * 1024);

			String text = new String(buffer);

			for (String line : text.split(System.lineSeparator())) {
				if (line.contains("recog")) {
					// System.out.println("line: " + line);
					line = URLDecoder.decode(line, "UTF-8");

					line = line.trim();

					Chatbot cb = new Chatbot();
					String[] recogResults = new String[line.split("recognitionResults\\[\\]=").length - 1];
					int i = 0;
					for (String test : line.split("recognitionResults\\[\\]=")) {
						if (test.length() == 0)
							continue;

						String result = test.substring(0, test.indexOf("&")).trim();
						// System.out.println("recognitionResult: "+ result);
						if (result.length() > 0) {
							recogResults[i] = result;
							i++;
						}
					}
					Double[] confidences = new Double[line.split("confidences\\[\\]=").length - 1];
					int j = 0;
					for (String test : line.split("confidences\\[\\]=")) {
						if (test.length() == 0 || test.contains("recog"))
							continue;

						String result = test.substring(0, test.indexOf("&")).trim();
						// System.out.println("confidence: " + result);
						if (result.length() > 0) {
							confidences[j] = Double.parseDouble(result);
							j++;
						}
					}

					boolean finalHypo = false;

					if (line.split("finalHypo=").length > 1)
						finalHypo = line.split("finalHypo=")[1].equals("final");

					cb.recognitionResults = recogResults;
					cb.confidences = confidences;
					cb.finalHypo = finalHypo;
					cb.wrapResultIntoIU();
				}
			}

			String html = "";
			File f = new File("index.html");
			html = new String(Files.readAllBytes(f.toPath()), StandardCharsets.UTF_8);
			OutputStreamWriter out = new OutputStreamWriter(s.getOutputStream());
			out.write(html);
			out.flush();
			out.close();

		}
	}

}
